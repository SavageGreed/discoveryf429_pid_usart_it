#include "main.h"
#include "UART_LL.h"

volatile uint16_t ReceiveBufferPosition = 0;
volatile uint8_t receiveBuffer[REC_BUFFER_SIZE] = {0x00};
volatile uint8_t errorCounter = 0;


//---------------------------------------------------------------------------------------
void UsartSendData(USART_TypeDef* UART_PORT, uint8_t* framePtr, uint16_t frameSize)
{
  uint16_t dataCounter = 0;

  while (dataCounter<frameSize)
  {
    while (!CHECK_IF_DATA_SEND_UART_FINISHED(UART_PORT)) { }
    LL_USART_TransmitData8(UART_PORT,*(uint8_t*)(framePtr + (dataCounter++)));
  }
}

uint16_t UartRecWholeBuffer(USART_TypeDef* UART_PORT, uint8_t* framePtr,
							uint16_t frameSize)
{
	uint16_t dataLoop = 0;
	uint16_t receiveDataCounter = 0;

	while(1)
	{
		volatile uint32_t timeout = 0;

		while (!CHECK_IF_DATA_RECEIVE(UART_PORT)) {
			timeout++;
			if(timeout == REC_TIMEOUT) { break; }
		}

		if(timeout == REC_TIMEOUT) { break; }
		else
		{
			*(framePtr + dataLoop) = (uint8_t)(UART_PORT->DR & 0x00FF);
			dataLoop++;
			receiveDataCounter++;

			if(receiveDataCounter == frameSize)
			{
				dataLoop = 0;
			}
		}
	}

	return receiveDataCounter;
}


/*
 * Use after initialization of UART
 * */
void UartEnableInterrupt(USART_TypeDef* UART_PORT)
{
	LL_USART_EnableIT_RXNE(UART_PORT);
	LL_USART_EnableIT_ERROR(UART_PORT);
}

