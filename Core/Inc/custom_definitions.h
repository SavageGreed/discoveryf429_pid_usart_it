
/**
  ******************************************************************************
  * @file           : custom_definitions.h
  * @version        : v1.0
  * @brief          : Header for serial access constants, data types and variables
  ******************************************************************************
  ******************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CUSTOM_DEFINITIONS_H__
#define __CUSTOM_DEFINITIONS_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/


/* Defines ------------------------------------------------------------------*/

 // returns bytes read (could be zero)
 // would be easy to make it end early on a stop char (e.g., \r or \n)
 // Setting Len to 0 implies reading data until FIFO is empty
 //static uint8_t VCP_read(uint8_t* Buf, uint32_t Len)
 //{
 //	uint32_t count=0;
 //	/* Check inputs */
 //	if ((Buf == NULL))// || (Len == 0))
 //	{
 //	 	return 0;
 //	}
 //
 //	while ((Len == 0) || (Len--))
 //	{
 //		if (RX_FIFO.head==RX_FIFO.tail) return count;
 //		count++;
 //		*Buf++=RX_FIFO.data[RX_FIFO.tail];
 //		RX_FIFO.tail=FIFO_INCR(RX_FIFO.tail);
 //	}
 //
 //	return count;
 //}
#define INITIAL_CENTRAL_PIEZO_VOLTAGE 0
#define CENTRAL_PIEZO_VOLTAGE 2047

#define CENTRAL_POSITION_CODE 8190
//#define CYCLES_PER_US 84
#define FPS 12.5f//60.0f
#define Max2(a,b) ((a>b)?a:b)

#define OutputSeq ((uint32_t)((1040.0f/TARGET_PERIOD_US) * 60.0f / FPS + 0.5f)*16U) //(( ((uint32_t)TARGET_PERIOD_US) * 1040 * 60)/FPS)
#define Packet_Per_TX	64U//64U //Number of data packets in each serial transmission
#define TX_Num		40U //40U//128U//250U//640U //256U//1024U
#define BatchLen (Packet_Per_TX * TX_Num) //8

//#define TARGET_PERIOD_US 2.0f //3.0f

#define TX_BUF_LEN (3*FIFO_SIZE+16)
#define Halfword_Per_Packet	(4U)
#define Byte_Per_Packet	(Halfword_Per_Packet * sizeof(uint16_t))
#define Serial_Batch_Byte_Len (Packet_Per_TX*Byte_Per_Packet) //must be greater than 4
#define Serial_Batch_Halfword_Len (Packet_Per_TX*Halfword_Per_Packet) //must be greater than 4

#define Serial_HalfWord_Len (ADC_num*2)
#define Serial_Packet_Byte_Len (Serial_HalfWord_Len*sizeof(uint16_t)*2)// + Serial_Float_Len*sizeof(float)) //must be greater than 4

#define CENTRAL_PIEZO_VOLTAGE 2047

#define CENTRAL_POSITION_CODE 8190



 #define FLOAT_DP 3

 //  float v1, v2;
 //  float v3, v4;

 #define USE_MULTI_ADC 0

 #define ADC_num 2
 #define ADC_ch_num 2

 //#define Data_num 2

 //#define Result_num (ADC_num*ADC_ch_num)
 //#define Buffer_len (ADC_ch_num*1)

 //#define LowB(x) (*((uint8_t)  &(##x)+1))
 //#define HighB(x) (*((uint8_t) &(##x)+0))
 #define HalfWordSel(x,sel) (*((uint16_t*) &(x)+(sel)))

 #define Min2(a,b) ((a<b)?a:b)
 #define Max2(a,b) ((a>b)?a:b)
 typedef union {
 	uint32_t Full;
 	uint16_t Half[2]; //index select ADC1 or ADC2
 } Data_Word_Union_T;


 typedef union {
 	uint32_t Full[ADC_num];
 	uint16_t Half[ADC_num*2]; //index select ADC1 or ADC2
 } Data_Array_Union_T;

 #define Serial_HalfWord_Len (ADC_num*2)
 #define Serial_Float_Len (2)
 //#define Serial_Packet_Byte_Len ((ADC_num*2)*sizeof(uint16_t) + 2*sizeof(float)) //must be greater than 4
 #define Serial_Packet_Byte_Len (Serial_HalfWord_Len*sizeof(uint16_t)*2)// + Serial_Float_Len*sizeof(float)) //must be greater than 4
 //#define Serial_Packet_Byte_Len (Serial_HalfWord_Len*sizeof(uint16_t)) //must be greater than 4
 typedef union {
 	uint8_t Bytes[Serial_Packet_Byte_Len];
 	uint16_t HalfWords[Serial_Packet_Byte_Len/2];
 	uint32_t Words[Serial_Packet_Byte_Len/4];
 	float Single[Serial_Packet_Byte_Len/4];
 } Serial_Packet_Union_T;




 #define CYCLES_PER_US 84
 const float us_conv = 1.0f/CYCLES_PER_US;
 const float s_conv = 1.0f/(1e6f * CYCLES_PER_US);

 #define TARGET_PERIOD_US 3.0f //3.0f

 const uint32_t HI_BOUND = (uint32_t)((TARGET_PERIOD_US+0.06f) * CYCLES_PER_US);
 const uint32_t LOW_BOUND = (uint32_t)((TARGET_PERIOD_US-0.06f) * CYCLES_PER_US);
 const uint32_t TARGET_PERIOD = (uint32_t)(TARGET_PERIOD_US * CYCLES_PER_US);
 #define HI_LOW_LIMIT 8


 /* should be multiple of 16 for best output accuracy in GUI*/

 #define FPS 12.5f//60.0f
 //#define K ((uint32_t)TARGET_PERIOD_US * 1040 * 60)
 #define OutputSeq ((uint32_t)((1040.0f/TARGET_PERIOD_US) * 60.0f / FPS + 0.5f)*16U) //(( ((uint32_t)TARGET_PERIOD_US) * 1040 * 60)/FPS)
 //4208//5104//5504 4200//26500 //27000(surfacebook)//5500 (60fps)//32000//22400 //11200 (new 30fps) //10784 //10672//106192 //10656 //106192 //6372 (50fps) //10619 (30fps)        //33278//100 //8

 const float mean_x16_conv = 16.0f/OutputSeq;
 const float mean_x4_conv = 4.0f/OutputSeq;
 const float mean_x1_conv = 1.0f/OutputSeq;
 const float mean_us_conv = (1.0f/OutputSeq)*(1.0f/CYCLES_PER_US);

 //#define MARGIN 512
 //#define TH_LOW (2048-MARGIN)
 //#define TH_HIGH (2048+MARGIN)
 #define INIT_DAC_OUT 0


 #define ADC_max 4095
 const float vsense = 3.3f/ADC_max;//3.3/ADC_max;



 //#define SeqNum 100 //8
 //typedef struct
 //{
 //	uint32_t TimeStamp[SeqNum];
 //	Data_Word_Union_T Value[SeqNum][ADC_num];   //ADC_num selects 1st or 2nd Channel (12 bit value)
 //
 //	//Data_Array_Union_T Value[SeqNum];
 //	//uint8_t FirstEdgeTimeIndex;
 //	//uint8_t LastEdgeTimeIndex;
 //} ADC_Result_T;

#define Halfword_Per_Packet	(4U)
#define Byte_Per_Packet	(Halfword_Per_Packet * sizeof(uint16_t))
#define Packet_Per_TX	64U//64U //Number of data packets in each serial transmission
#define TX_Num		40U //40U//128U//250U//640U //256U//1024U
#define BatchByteLen (Packet_Per_TX * TX_Num * Byte_Per_Packet) //8
#define BatchLen (Packet_Per_TX * TX_Num) //8
#define Serial_Batch_Byte_Len (Packet_Per_TX*Byte_Per_Packet) //must be greater than 4
#define Serial_Batch_Halfword_Len (Packet_Per_TX*Halfword_Per_Packet) //must be greater than 4
 typedef struct
 {
 	uint16_t dT;	//Actual time step
 	uint16_t IMax;   //Max Intensity
 	uint16_t YPos;   //Y position
 	uint16_t PID_Out; //PID output
 } PID_Data_T;




 typedef union {
 	uint8_t Bytes[Serial_Batch_Byte_Len];
 	uint16_t HalfWords[Serial_Batch_Halfword_Len];
 } Serial_Batch_Union_T;


 typedef struct
 {
 //	uint32_t TimeStamp;
 	uint32_t MaxPeriod; //us
 //	uint32_t MaxPeriodInd;

 	uint32_t MinPeriod; //us
 //	uint32_t MinPeriodInd;

 	uint16_t HiCnt;
 	uint16_t LowCnt;
 //	uint32_t HiPeriod[HI_LOW_LIMIT]; //us
 //	uint32_t LowPeriod[HI_LOW_LIMIT]; //us
 //	uint32_t HiPeriodInd[HI_LOW_LIMIT]; //us
 //	uint32_t LowPeriodInd[HI_LOW_LIMIT]; //us

 	int32_t PeriodShift;
 	uint32_t SumPeriod; //us

 //	Data_Array_Union_T MaxValue;
 //	Data_Array_Union_T MinValue;
 	uint32_t SumValue[ADC_num*2];

 } ADC_Stats_T;








#ifdef __cplusplus
}
#endif

#endif /* __CUSTOM_DEFINITIONS_H__ */

/*****************************END OF FILE****/
