/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "UART_LL.h"
#include "string.h"
#include "stdio.h"
#include "custom_usb_virtual_serial.h" //"usbd_cdc_if.h"
#include "stm32f4xx.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
DMA_HandleTypeDef hdma_adc1;

DAC_HandleTypeDef hdac;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim7;

/* USER CODE BEGIN PV */

#define UART_DMA_BUFFER_SIZE 512

static uint8_t buffer[UART_DMA_BUFFER_SIZE];


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM3_Init(void);
static void MX_ADC2_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM7_Init(void);
/* USER CODE BEGIN PFP */


//void custom_uart_callback(UART_HandleTypeDef *huart);
static void MicroSec_Counter_Init(void);
__always_inline static inline uint32_t Get_MicroSec_Count(void);
//__always_inline inline float bytes_to_float(uint8_t *s);
//__always_inline inline float bytes_to_uint16(uint8_t *s);


__IO uint8_t ubReceive = 0;
__IO uint8_t ubSend = 0;
__IO uint8_t ubUSART1TransmissionComplete = 0;
__IO uint8_t ubUSART1ReceptionComplete = 0;



void BufferTransfer(uint8_t *pData, uint16_t Size);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


const uint16_t saw_tooth_array[16] = {
		256,
		512,
		768,
		1024,
		1280,
		1536,
		1792,
		2048,
		2304,
		2560,
		2816,
		3072,
		3328,
		3584,
		3840,
		4095
	};

volatile SERIAL_CMD_t COM_Mode = CMD_STOP_ALL;
volatile SERIAL_CMD_t COM_Mode_SYNC = CMD_STOP_ALL;
volatile uint8_t Get_Batch = 0;

uint8_t TestInputMask[4] = {0};


uint8_t Reset_PID = 1;
#define CENTRAL_PIEZO_VOLTAGE 2047

#define CENTRAL_POSITION_CODE 8190

PID_t PID_Param =      {.TargetPos =CENTRAL_POSITION_CODE, .nT = 1, .Inv_nT = 1.0f, .P=0.0f, .I=0.0f, .D=0.0f, .Update = 0, .Enable = 0}; //.u_init=0.0f, .e_init=0.0f,
PID_t PID_Param_SYNC = {.TargetPos =CENTRAL_POSITION_CODE, .nT = 1, .Inv_nT = 1.0f, .P=0.0f, .I=0.0f, .D=0.0f, .Update = 0, .Enable = 0};

BaseVoltage_t NormVout = {.code = CENTRAL_PIEZO_VOLTAGE, .Update = 0};


#define FLOAT_DP 3
uint32_t ClkFreq = 0;

#define USE_MULTI_ADC 0

#define ADC_num 2
#define ADC_ch_num 2

#define HalfWordSel(x,sel) (*((uint16_t*) &(x)+(sel)))

#define Min2(a,b) ((a<b)?a:b)
#define Max2(a,b) ((a>b)?a:b)

typedef union {
	uint32_t Full;
	uint16_t Half[2]; //index select ADC1 or ADC2
} Data_Word_Union_T;


typedef union {
	uint32_t Full[ADC_num];
	uint16_t Half[ADC_num*2]; //index select ADC1 or ADC2
} Data_Array_Union_T;

#define Serial_HalfWord_Len (ADC_num*2)
#define Serial_Float_Len (2)
#define Serial_Packet_Byte_Len (Serial_HalfWord_Len*sizeof(uint16_t)*2)// + Serial_Float_Len*sizeof(float)) //must be greater than 4

typedef union {
	uint8_t Bytes[Serial_Packet_Byte_Len];
	uint16_t HalfWords[Serial_Packet_Byte_Len/2];
	uint32_t Words[Serial_Packet_Byte_Len/4];
	float Single[Serial_Packet_Byte_Len/4];
} Serial_Packet_Union_T;

Serial_Packet_Union_T TX_Data;


#define CYCLES_PER_US 84
const float us_conv = 1.0f/CYCLES_PER_US;
const float s_conv = 1.0f/(1e6f * CYCLES_PER_US);

#define TARGET_PERIOD_US 4.0f //3.0f

const uint32_t HI_BOUND = (uint32_t)((TARGET_PERIOD_US+0.06f) * CYCLES_PER_US);
const uint32_t LOW_BOUND = (uint32_t)((TARGET_PERIOD_US-0.06f) * CYCLES_PER_US);
const uint32_t TARGET_PERIOD = (uint32_t)(TARGET_PERIOD_US * CYCLES_PER_US);
#define HI_LOW_LIMIT 8


/* should be multiple of 16 for best output accuracy in GUI*/

#define FPS 12.5f//60.0f
//#define K ((uint32_t)TARGET_PERIOD_US * 1040 * 60)
#define OutputSeq ((uint32_t)((1040.0f/TARGET_PERIOD_US) * 60.0f / FPS + 0.5f)*16U) //(( ((uint32_t)TARGET_PERIOD_US) * 1040 * 60)/FPS)
//4208//5104//5504 4200//26500 //27000(surfacebook)//5500 (60fps)//32000//22400 //11200 (new 30fps) //10784 //10672//106192 //10656 //106192 //6372 (50fps) //10619 (30fps)        //33278//100 //8

const float mean_x16_conv = 16.0f/OutputSeq;
const float mean_x4_conv = 4.0f/OutputSeq;
const float mean_x1_conv = 1.0f/OutputSeq;
const float mean_us_conv = (1.0f/OutputSeq)*(1.0f/CYCLES_PER_US);

//#define MARGIN 512
//#define TH_LOW (2048-MARGIN)
//#define TH_HIGH (2048+MARGIN)
#define INIT_DAC_OUT 0


#define ADC_max 4095
const float vsense = 3.3f/ADC_max;//3.3/ADC_max;

uint8_t DataReady = 0;

//#define SeqNum 100 //8
//typedef struct
//{
//	uint32_t TimeStamp[SeqNum];
//	Data_Word_Union_T Value[SeqNum][ADC_num];   //ADC_num selects 1st or 2nd Channel (12 bit value)
//
//	//Data_Array_Union_T Value[SeqNum];
//	//uint8_t FirstEdgeTimeIndex;
//	//uint8_t LastEdgeTimeIndex;
//} ADC_Result_T;

#define Halfword_Per_Packet	(4U)
#define Byte_Per_Packet	(Halfword_Per_Packet * sizeof(uint16_t))
#define Packet_Per_TX	64U//64U //Number of data packets in each serial transmission
#define TX_Num		20U //40U//128U//250U//640U //256U//1024U

#define BatchByteLen (Packet_Per_TX * TX_Num * Byte_Per_Packet) //8

#define BatchLen (Packet_Per_TX * TX_Num) //8
typedef struct
{
	uint16_t dT;	//Actual time step
	uint16_t IMax;   //Max Intensity
	uint16_t YPos;   //Y position
	uint16_t PID_Out; //PID output
} PID_Data_T;
volatile uint8_t BatchFilled = 0;
volatile PID_Data_T ResultBatch[BatchLen];


#define Serial_Batch_Byte_Len (Packet_Per_TX*Byte_Per_Packet) //must be greater than 4
#define Serial_Batch_Halfword_Len (Packet_Per_TX*Halfword_Per_Packet) //must be greater than 4
typedef union {
	uint8_t Bytes[Serial_Batch_Byte_Len];
	uint16_t HalfWords[Serial_Batch_Halfword_Len];
} Serial_Batch_Union_T;

Serial_Batch_Union_T TX_Batch_Data;


typedef struct
{
//	uint32_t TimeStamp;
	uint32_t MaxPeriod; //us
//	uint32_t MaxPeriodInd;

	uint32_t MinPeriod; //us
//	uint32_t MinPeriodInd;

	uint16_t HiCnt;
	uint16_t LowCnt;
//	uint32_t HiPeriod[HI_LOW_LIMIT]; //us
//	uint32_t LowPeriod[HI_LOW_LIMIT]; //us
//	uint32_t HiPeriodInd[HI_LOW_LIMIT]; //us
//	uint32_t LowPeriodInd[HI_LOW_LIMIT]; //us

	int32_t PeriodShift;
	uint32_t SumPeriod; //us

//	Data_Array_Union_T MaxValue;
//	Data_Array_Union_T MinValue;
	uint32_t SumValue[ADC_num*2];

} ADC_Stats_T;

volatile ADC_Stats_T adc_stats;
volatile ADC_Stats_T result_stats;
volatile uint8_t result_ready = 0;

//ADC_Result_T adc_results;

//uint32_t
//Data_Array_Union_T
Data_Array_Union_T buf;

volatile uint32_t seq = 0;//OutputSeq; //0;
uint16_t PosEdgeCnt = 0;

//uint32_t time_us = 0, prev_time_us = 0, period_us = 0;

__always_inline static inline void Init_ADC_Stats(volatile ADC_Stats_T * stats) {
	stats->MinPeriod = 0xffffffff;
	stats->MaxPeriod = 0;
	stats->HiCnt = 0;
	stats->LowCnt = 0;
	stats->PeriodShift = 0;
	stats->SumPeriod = 0;

//	stats->MinValue.Full[0] = 0xffffffff;
//	stats->MinValue.Full[1] = 0xffffffff;
//	stats->MaxValue.Full[0] = 0;
//	stats->MaxValue.Full[1] = 0;

	stats->SumValue[0] = 0; //ADC1 CH1
	stats->SumValue[1] = 0; //ADC1 CH2
	stats->SumValue[2] = 0; //ADC2 CH1
	stats->SumValue[3] = 0; //ADC2 CH2
}






Serial_Batch_Union_T TX_Batch_Data;
Serial_Packet_Union_T TX_Data;



volatile ADC_Stats_T adc_stats;
volatile ADC_Stats_T result_stats;

volatile PID_Data_T ResultBatch[BatchLen];
//
//__always_inline inline float bytes_to_float(uint8_t *s) {
//	float f;
//	memcpy(&f, s, sizeof(f));
//	return f;
//	//*((float *) &temp);
//}
//
// __always_inline inline float bytes_to_uint16(uint8_t *s) {
//	uint16_t v;
//	memcpy(&v, s, sizeof(v));
//	return v;
//}



__always_inline static inline uint16_t __pid_controller(const uint32_t y, const float Inv_MA_n, const float dt, //const int16_t out_max, const int16_t out_min,
		const uint16_t Out_Offset, const PID_t param, const uint8_t reset) { //,  const float out_i0, const float err_0) {


//	y:		Measured Output of the System
//	y_ref:	Desired Output of the System
//	dt:		Sampling Time
//	out_max:	Output maximum limit
//	out_min:	Output minimum limit
//	Kp:		Controller Proportional Gain Constant
//	Ki:		Controller Integral Gain Constant
//	Kd:		Controller Differential Gain Constant
//	out_i0:	Initial state of the integrator
//	err_0:	Initial error


	static float sum_i = 0.0f;
	//static int16_t prev_err = 0;
	static float prev_err = 0.0f;

//	//static uint32_t k;

// ******* Error between the reference and actual output *********
	//int16_t y_err = y_ref - y;
	//int16_t y_err = param.TargetPos - y;
	float y_err = param.TargetPos - y * Inv_MA_n; //param.Inv_nT;

// ****** Initialization ********
	if (reset || param.Update) {
		//k = 0; // Step variable
		sum_i = 0.0f; //param.u_init; //out_i0;	  //0.0f
		prev_err = y_err; //param.e_init; //err_0; //0.0f
	}

// ******* Integrate error over time *********
	sum_i += (param.I * y_err)*dt;// /1000000.0f ;

// ******* Calculate differential term ************
	//float out_d = param.D * (err - err_prev) / dt;
	float out_d = (y_err - prev_err)*(param.D / dt);
	//if (out_d > 1023.0f) out_d = 1023.f;
	//else if (out_d < -1023.0f) out_d = -1023.f;
	prev_err = y_err;

//	int16_t output = param.P * err;

//	if (output>out_max) output = out_max;
//	else if (output<out_min) output = out_min;
	float output =  0.5f + Out_Offset + (param.P * y_err + sum_i + out_d);
	if (output<1.0f) output = 0.0f;
	else if (output>4095.0f) output = 4095.0f;

	return output; //param.P * y_err + sum_i + out_d;

}


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	//HAL_GPIO_WritePin(IntGen_GPIO_Port, IntGen_Pin, GPIO_PIN_SET);
	static uint32_t prev_time_us = 0;
	static uint32_t prev_PID_time = 0;
	static uint16_t PID_out = 0;
	static uint8_t CycleCount = 0;

	static uint16_t BatchCount = 0;

	uint16_t X_Pos = 0, Y_Pos = 0;
	static uint32_t Y_sum = 0;

	uint32_t time_us = Get_MicroSec_Count();

	uint32_t period_us = time_us - prev_time_us;

	if (COM_Mode_SYNC == CMD_MODE_BIN || COM_Mode_SYNC == CMD_MODE_TXT) {

		adc_stats.SumValue[0] += buf.Half[0];
		adc_stats.SumValue[1] += buf.Half[2];
		adc_stats.SumValue[2] += buf.Half[1];
		adc_stats.SumValue[3] += buf.Half[3];

	} else if (COM_Mode_SYNC == CMD_MODE_POS || COM_Mode_SYNC == CMD_MODE_BATCH) {
		uint16_t max_a, max_b, max_out;


		if (buf.Half[0]>buf.Half[1]) {
			max_a = buf.Half[0];

		} else {
			max_a = buf.Half[1];

		}
		if (buf.Half[2]>buf.Half[3]) {
			max_b = buf.Half[2];

		} else {
			max_b = buf.Half[3];

		}

		max_out = Max2(max_a, max_b);
		adc_stats.SumValue[0] += max_out; //Max2(max_a, max_b);


		X_Pos = (CENTRAL_POSITION_CODE + buf.Half[2]+buf.Half[3])-(buf.Half[0]+buf.Half[1]);
		Y_Pos = (CENTRAL_POSITION_CODE + buf.Half[1]+buf.Half[3])-(buf.Half[0]+buf.Half[2]);

		adc_stats.SumValue[2] += Y_Pos;
		adc_stats.SumValue[3] += X_Pos;


		if (PID_Param.Enable){


			if (CycleCount==0) {
				Y_sum = Y_Pos;

			} else {
				Y_sum += Y_Pos;

			}


			if ((CycleCount < (PID_Param.nT - 1U))) {
				adc_stats.SumValue[1] += PID_out;
				CycleCount++;
			} else {

				PID_out = __pid_controller(Y_sum /*Y_MA*/, PID_Param.Inv_nT /** Inv_MA_Num*/, (time_us - prev_PID_time)*s_conv, NormVout.code, PID_Param, Reset_PID);

				HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, (uint32_t)PID_out);

				adc_stats.SumValue[1] += PID_out; //((2047+8190) - Y_Pos);//PID_out;

				Reset_PID = 0;
				CycleCount = 0;
				PID_Param.Update = 0;
				prev_PID_time = time_us;
			}


		} else {
			Reset_PID = 1;
			CycleCount = 0;
			HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, (uint32_t)NormVout.code);
		}

		if (Get_Batch == 1 && !BatchFilled) {

			ResultBatch[BatchCount].dT = period_us;
			ResultBatch[BatchCount].IMax = max_out;
			ResultBatch[BatchCount].YPos = Y_Pos;
			ResultBatch[BatchCount].PID_Out = (PID_Param.Enable)?PID_out:NormVout.code;

			if (BatchCount<(BatchLen-1)) {
				BatchCount++;
			} else {
				BatchFilled = 1;
					BatchCount=0;
			}
			adc_stats.SumValue[0]=0;
			adc_stats.SumValue[1]=0;
			adc_stats.SumValue[2]=0;
			adc_stats.SumValue[3]=0;

		}


	}


	if (COM_Mode_SYNC!=CMD_STOP_ALL) {
		if (period_us > adc_stats.MaxPeriod) {
			adc_stats.MaxPeriod = period_us;

		}
		if (period_us < adc_stats.MinPeriod) {
			adc_stats.MinPeriod = period_us;

		}

		adc_stats.PeriodShift += (period_us-TARGET_PERIOD);
		adc_stats.SumPeriod += period_us;
	}

	prev_time_us = time_us;

	if (seq<(OutputSeq-1)) {
		seq++;
	} else {

		seq = 0;
		Reset_PID = 0;
		result_stats = adc_stats;
		Init_ADC_Stats(&adc_stats);
		result_ready = (COM_Mode_SYNC != CMD_STOP_ALL && COM_Mode_SYNC != CMD_MODE_BATCH); //? 1 : 0;

		COM_Mode_SYNC = COM_Mode;
	}

	//osSemaphoreRelease(binarySemAnalogHandle);

//	HAL_GPIO_WritePin(IntGen_GPIO_Port, IntGen_Pin, GPIO_PIN_RESET);
}

static void MicroSec_Counter_Init(void)
{
	ClkFreq = HAL_RCC_GetPCLK2Freq();
	//__HAL_RCC_TIM5_CLK_DISABLE();
	__HAL_RCC_TIM5_CLK_ENABLE();
	//RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;
	  TIM5->PSC = 0; //(ClkFreq / 10000000) - 1;
	  TIM5->ARR = 0xffffffff;
	  TIM5->CR1 |= TIM_CR1_CEN;// | TIM_CR1_DIR;
	  TIM5->CNT = 0xffffffff; /*forces a reset on next increment to update prescaler (not sure why this is necessary)*/


	  //__HAL_RCC_TIM5_CLK_DISABLE();
	  //__HAL_RCC_TIM5_CLK_ENABLE();

//	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
//	DWT->CYCCNT = 0;
//	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;


}

__always_inline static inline uint32_t Get_MicroSec_Count(void)
{
	return (TIM5->CNT); // << 1;

	//return ClkFreq;
//	return DWT->CYCCNT;
}



//void USER_UART_IRQHandler(UART_HandleTypeDef *huart)
//{
//    if(USART1 == huart1.Instance)                                   //Determine whether it is serial port 1
//    {
//        if(RESET != __HAL_UART_GET_FLAG(&huart1, UART_FLAG_IDLE))   //Judging whether it is idle interruption
//        {
//            __HAL_UART_CLEAR_IDLEFLAG(&huart1);                     //Clear idle interrupt sign (otherwise it will continue to enter interrupt)
//            //printf("\r\nUART1 Idle IQR Detected\r\n");
//            custom_uart_callback(huart);                          //Call interrupt handler
//        }
//    }
//}

//extern uint8_t receive_buff[255];
//my buffffffff
//HAL_UART_Receive_DMA(&huart1, buffer, UART_DMA_BUFFER_SIZE);
//void custom_uart_callback(UART_HandleTypeDef *huart)
//{
//	//Stop this DMA transmission
//    HAL_UART_DMAStop(&huart1);
//    size_t len =0;
//    //Calculate the length of the received data
//    //uint8_t data_length  = BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);
//    //uint8_t data_length  = UART_DMA_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);
//
//    uint8_t data_length = UART_DMA_BUFFER_SIZE - huart1.hdmarx->Instance->NDTR;
//
//	//Test function: Print out the received data
//    //printf("Receive Data(length = %d): ",data_length);
//    HAL_UART_Transmit(&huart1,buffer,data_length,0x200);
//    //printf("\r\n");
//    //len
//    len=data_length;
//
//    //start
//    if( (len==CMD_START_BIN_STR_LEN) && (strncmp((const char *)buffer, CMD_START_BIN_STR, CMD_START_BIN_STR_LEN)==0) ) {
//   	  		COM_Mode = CMD_MODE_BIN;
//   	  	} else if ( (len==CMD_START_POS_STR_LEN) && (strncmp((const char *)buffer, CMD_START_POS_STR, CMD_START_POS_STR_LEN)==0) ) {
//   	  		COM_Mode = CMD_MODE_POS;
//   	  	} else if ( (len==CMD_START_BATCH_STR_LEN) && (strncmp((const char *)buffer, CMD_START_BATCH_STR, CMD_START_BATCH_STR_LEN)==0) ) {
//   	  		COM_Mode = CMD_MODE_BATCH;
//   	  		Get_Batch = 1;
//   	  	} else if ( (len==CMD_START_TXT_STR_LEN) && (strncmp((const char *)buffer, CMD_START_TXT_STR, CMD_START_TXT_STR_LEN)==0) ) {
//   	  		COM_Mode = CMD_MODE_TXT;
//   	  	} else if  (len==(CMD_EN_PID_LEN+1) && (strncmp((const char *)buffer, CMD_EN_PID, CMD_EN_PID_LEN)==0)) {
//   	  		PID_Param.Enable = buffer[CMD_EN_PID_LEN];
//   	  		len=CMD_EN_PID_LEN;
//   	  	} else if  (len==(CMD_SET_PID_LEN + PID_PARAM_LEN) && (strncmp((const char *)buffer, CMD_SET_PID, CMD_SET_PID_LEN)==0)) {
//
//   	  		PID_Param.nT = buffer[CMD_SET_PID_LEN];
//   	  		PID_Param.Inv_nT = 1.0f/buffer[CMD_SET_PID_LEN];
//   	  		PID_Param.P = bytes_to_float(&buffer[CMD_SET_PID_LEN+1]);
//   	  		PID_Param.I = bytes_to_float(&buffer[CMD_SET_PID_LEN+1+FLOAT_LEN]);
//   	  		PID_Param.D = bytes_to_float(&buffer[CMD_SET_PID_LEN+1+FLOAT_LEN*2]);
//   	  		PID_Param.Update = 1;
//   	  		len=CMD_SET_PID_LEN;
//
//   	  	} else if  (len==(CMD_SET_VOUT_LEN + VOUT_CODE_LEN) && (strncmp((const char *)buffer, CMD_SET_VOUT, CMD_SET_VOUT_LEN)==0)) {
//
//   	  		NormVout.code = bytes_to_uint16(&buffer[CMD_SET_VOUT_LEN]);
//   	  		NormVout.Update = 1;
//   	  		len=CMD_SET_VOUT_LEN;
//
//   	  	} else if  (len==(CMD_SET_YPOS_LEN + YPOS_CODE_LEN) && (strncmp((const char *)buffer, CMD_SET_YPOS, CMD_SET_YPOS_LEN)==0)) {
//
//   	  		PID_Param.TargetPos = bytes_to_uint16(&buffer[CMD_SET_YPOS_LEN]);
//   	  		//PID_Param.Update = 1;
//   	  		len=CMD_SET_YPOS_LEN;
//
//   	  	} else {
//   	  		COM_Mode = CMD_STOP_ALL;
//   	  	}
//  // len=0;
////}
//
//    //enddd
//
//	//Zero Receiving Buffer
//    memset(buffer,0,data_length);
//    data_length = 0;
//
//    //Restart to start DMA transmission of 255 bytes of data at a time
//    HAL_UART_Receive_DMA(&huart1, (uint8_t*)buffer, UART_DMA_BUFFER_SIZE);
//}


#define TX_BUF_LEN (3*FIFO_SIZE+16)

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_ADC2_Init();
  MX_DAC_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */


	Init_ADC_Stats(&adc_stats);
	// Start ADC,DMA and timer of ADC
	HAL_ADC_Start(&hadc2);
	HAL_ADCEx_MultiModeStart_DMA(&hadc1, buf.Full, ADC_num);
	MicroSec_Counter_Init();
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);



	HAL_TIM_Base_Start(&htim7);
	HAL_DAC_Start(&hdac, DAC_CHANNEL_2);

	UartEnableInterrupt(USART1);

	 //__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);  // Enable serial port idle interrupt

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	 char tx_buf[TX_BUF_LEN] = {0}; //= "Testing Virtual Com Port.\n"; //STM32 -> PC
	 //uint8_t rx_buf[FIFO_SIZE]= {0};

	 uint16_t str_len = 0;
	 uint16_t k=0;
	 uint16_t n=0;
  while (1)
  {
		if(COM_Mode_SYNC == CMD_STOP_ALL) {
				//HAL_Delay (1);
				continue;
			} else if (BatchFilled==1) {
				//result_ready = 0;
				//HAL_Delay (1);
			} else if (result_ready==1 && COM_Mode_SYNC != CMD_MODE_BATCH) {
				//HAL_Delay (1);
				result_ready = 0;
			} else {
				//HAL_Delay (1);
				continue;
			}


			if (COM_Mode_SYNC == CMD_MODE_BIN) {


			//************************ <Begin> Binary Output to Serial via USB CDC ******************************************
			//	for (int i=0; i<(ADC_num*2); i++){
			//		TX_Data.HalfWords[i] =  (uint16_t)(result_stats.SumValue[i]/(OutputSeq>>4));
			//	}
				TX_Data.HalfWords[0] =  (uint16_t)(result_stats.SumValue[0]/(OutputSeq>>4));
				TX_Data.HalfWords[1] =  (uint16_t)(result_stats.SumValue[1]/(OutputSeq>>4));
				TX_Data.HalfWords[2] =  (uint16_t)(result_stats.SumValue[2]/(OutputSeq>>4));
				TX_Data.HalfWords[3] =  (uint16_t)(result_stats.SumValue[3]/(OutputSeq>>4));

				TX_Data.HalfWords[4] = (uint16_t)(result_stats.SumPeriod/(OutputSeq>>4));
				TX_Data.HalfWords[5] = result_stats.PeriodShift;
				TX_Data.HalfWords[6] = result_stats.MinPeriod;
				TX_Data.HalfWords[7] = result_stats.MaxPeriod;
				//TX_Data.HalfWords[ADC_num*2] = 0xFF80; //terminator
				//TX_Data.HalfWords[ADC_num*2+1] = 0xFE81; //terminator
				//CDC_Transmit_FS(TX_Data.Bytes, Serial_Packet_Byte_Len);
				// HAL_UART_Transmit_DMA(&huart1, TX_Data.Bytes, Serial_Packet_Byte_Len);

				  //BufferTransfer(TX_Data.Bytes,Serial_Packet_Byte_Len);
				UsartSendData(USART1,TX_Data.Bytes,Serial_Packet_Byte_Len);



				//k++;
			//************************ <End> Binary Output to Serial via USB CDC ******************************************
			} else if (COM_Mode_SYNC == CMD_MODE_POS) {
			//************************ <Begin> MaxMin & Position Output to Serial via USB CDC ******************************************
			//	for (int i=0; i<(ADC_num); i++){
			//		TX_Data.HalfWords[i] =  (uint16_t)(result_stats.SumValue[i]/(OutputSeq>>4));
			//	}
			//	for (int i=ADC_num; i<(ADC_num*2); i++){
			//		TX_Data.HalfWords[i] =  (uint16_t)(result_stats.SumValue[i]/(OutputSeq>>2));
			//	}
				TX_Data.HalfWords[0] =  (uint16_t)(result_stats.SumValue[0]/(OutputSeq>>4));
				TX_Data.HalfWords[1] =  (uint16_t)(result_stats.SumValue[1]/(OutputSeq>>4));
				TX_Data.HalfWords[2] =  (uint16_t)(result_stats.SumValue[2]/(OutputSeq>>2));
				TX_Data.HalfWords[3] =  (uint16_t)(result_stats.SumValue[3]/(OutputSeq>>2));

				TX_Data.HalfWords[4] = (uint16_t)(result_stats.SumPeriod/(OutputSeq>>4));
				TX_Data.HalfWords[5] = result_stats.PeriodShift;
				TX_Data.HalfWords[6] = result_stats.MinPeriod;
				TX_Data.HalfWords[7] = result_stats.MaxPeriod;
			//	TX_Data.Single[2] = result_stats.PeriodShift;
			//	TX_Data.Single[3] = result_stats.MinPeriod;
				//CDC_Transmit_FS(TX_Data.Bytes, Serial_Packet_Byte_Len);
				//HAL_UART_Transmit_DMA(&huart1, TX_Data.Bytes, Serial_Packet_Byte_Len);
				//BufferTransfer(TX_Data.Bytes,Serial_Packet_Byte_Len);
				UsartSendData(USART1,TX_Data.Bytes,Serial_Packet_Byte_Len);
				//************************ <End> MaxMin & Position Output to Serial via USB CDC ******************************************
			} else if (BatchFilled && Get_Batch) {

				int j = 0;
				for (int i=0; i<(Packet_Per_TX); i++){
					TX_Batch_Data.HalfWords[j] = ResultBatch[n+i].dT;
					TX_Batch_Data.HalfWords[j+1] = ResultBatch[n+i].IMax;
					TX_Batch_Data.HalfWords[j+2] = ResultBatch[n+i].PID_Out;
					TX_Batch_Data.HalfWords[j+3] = ResultBatch[n+i].YPos;
					j += Halfword_Per_Packet;
				}
				//CDC_Transmit_FS(TX_Batch_Data.Bytes, Serial_Batch_Byte_Len);
				//HAL_UART_Transmit_DMA(&huart1, TX_Batch_Data.Bytes, Serial_Batch_Byte_Len);
				//BufferTransfer(TX_Batch_Data.Bytes,Serial_Batch_Byte_Len);

				UsartSendData(USART1,TX_Batch_Data.Bytes,Serial_Batch_Byte_Len);
			//	str_len = siprintf(tx_buf,"%u - %u, %u, %u, %u\n",
			//			(unsigned int)n,
			//			(unsigned int)ResultBatch[n].YPos,
			//			(unsigned int)ResultBatch[n+1].YPos,
			//			(unsigned int)ResultBatch[n+2].YPos,
			//			(unsigned int)ResultBatch[n+3].YPos
			//			);
			//	CDC_Transmit_FS((uint8_t *)tx_buf, str_len);


				if (n<(BatchLen-Packet_Per_TX)) {
					n += Packet_Per_TX;
				} else {
					n = 0;
					//BatchFilled = 0;
					Get_Batch = 0;
					BatchFilled = 0;
					//Batch_TX_Done = 1;
				}


			} else if (COM_Mode_SYNC == CMD_MODE_TXT) {
			//************************ <Begin> ASCII Text Output to Serial via USB CDC ********************************************


				str_len = sprintf(tx_buf,"%u - T: %.3f, S:%.3f, Max: %.3f Min: %.3f\n",
						(unsigned int)k,
						result_stats.SumPeriod*mean_us_conv,
						result_stats.PeriodShift*us_conv,
						result_stats.MaxPeriod*us_conv,
						result_stats.MinPeriod*us_conv
						);

				//CDC_Transmit_FS((uint8_t *)tx_buf, str_len);
				//HAL_UART_Transmit_DMA(&huart1, (uint8_t *)tx_buf,  str_len);
				//BufferTransfer((uint8_t *)tx_buf,str_len);
				UsartSendData(USART1,(uint8_t *)tx_buf,str_len);
				k++;
			//************************ <End> ASCII Text Output to Serial via USB CDC ********************************************
			}





		  //HAL_UART_Transmit_DMA(&huart1, dma_buffer, 2000);






    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T3_CC1;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode 
  */
  multimode.Mode = ADC_DUALMODE_REGSIMULT;
  multimode.DMAAccessMode = ADC_DMAACCESSMODE_2;
  multimode.TwoSamplingDelay = ADC_TWOSAMPLINGDELAY_5CYCLES;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.ScanConvMode = ENABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 2;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization 
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT2 config 
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 251;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 42;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  __HAL_TIM_DISABLE_OCxPRELOAD(&htim3, TIM_CHANNEL_1);
  /* USER CODE BEGIN TIM3_Init 2 */

  htim3.Init.Period = TARGET_PERIOD - 1;//251;

  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 0;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 2099;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
  
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  /**USART1 GPIO Configuration  
  PA9   ------> USART1_TX
  PA10   ------> USART1_RX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_9|LL_GPIO_PIN_10;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_7;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USART1 DMA Init */
  
  /* USART1_RX Init */
  LL_DMA_SetChannelSelection(DMA2, LL_DMA_STREAM_2, LL_DMA_CHANNEL_4);

  LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_STREAM_2, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

  LL_DMA_SetStreamPriorityLevel(DMA2, LL_DMA_STREAM_2, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA2, LL_DMA_STREAM_2, LL_DMA_MODE_CIRCULAR);

  LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_STREAM_2, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_STREAM_2, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA2, LL_DMA_STREAM_2, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA2, LL_DMA_STREAM_2, LL_DMA_MDATAALIGN_BYTE);

  LL_DMA_DisableFifoMode(DMA2, LL_DMA_STREAM_2);

  /* USART1_TX Init */
  LL_DMA_SetChannelSelection(DMA2, LL_DMA_STREAM_7, LL_DMA_CHANNEL_4);

  LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_STREAM_7, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

  LL_DMA_SetStreamPriorityLevel(DMA2, LL_DMA_STREAM_7, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA2, LL_DMA_STREAM_7, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_STREAM_7, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_STREAM_7, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA2, LL_DMA_STREAM_7, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA2, LL_DMA_STREAM_7, LL_DMA_MDATAALIGN_BYTE);

  LL_DMA_DisableFifoMode(DMA2, LL_DMA_STREAM_7);

  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),10, 0));
  NVIC_EnableIRQ(USART1_IRQn);

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_ConfigAsyncMode(USART1);
  LL_USART_Enable(USART1);
  /* USER CODE BEGIN USART1_Init 2 */



  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream2_IRQn interrupt configuration */
  NVIC_SetPriority(DMA2_Stream2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),10, 0));
  NVIC_EnableIRQ(DMA2_Stream2_IRQn);
  /* DMA2_Stream7_IRQn interrupt configuration */
  NVIC_SetPriority(DMA2_Stream7_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),10, 0));
  NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(VHigh_GPIO_Port, VHigh_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, IntGen_Pin|LED_GREEN_Pin|LED_RED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : VHigh_Pin */
  GPIO_InitStruct.Pin = VHigh_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(VHigh_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PD12 PD13 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : IntGen_Pin */
  GPIO_InitStruct.Pin = IntGen_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(IntGen_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PG6 PG7 PG11 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : PG10 PG12 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF9_LTDC;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_GREEN_Pin LED_RED_Pin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin|LED_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */




void BufferTransfer(uint8_t *pData, uint16_t Size)
{
	uint8_t ubSend = 0;
  /* Send characters one per one, until last char to be sent */
  while (ubSend < Size)
  {
#if (USE_TIMEOUT == 1)
    Timeout = USART_SEND_TIMEOUT_TXE_MS;
#endif /* USE_TIMEOUT */

    /* Wait for TXE flag to be raised */
    while (!LL_USART_IsActiveFlag_TXE(USART1))
    {
#if (USE_TIMEOUT == 1)
      /* Check Systick counter flag to decrement the time-out value */
      if (LL_SYSTICK_IsActiveCounterFlag())
      {
        if(Timeout-- == 0)
        {
          /* Time-out occurred. Set LED to blinking mode */
          LED_Blinking(LED_BLINK_SLOW);
        }
      }
#endif /* USE_TIMEOUT */
    }

    /* If last char to be sent, clear TC flag */
    if (ubSend == (Size - 1))
    {
      LL_USART_ClearFlag_TC(USART1);
    }

    /* Write character in Transmit Data register.
       TXE flag is cleared by writing data in DR register */
    LL_USART_TransmitData8(USART1, pData[ubSend++]);
  }

#if (USE_TIMEOUT == 1)
  Timeout = USART_SEND_TIMEOUT_TC_MS;
#endif /* USE_TIMEOUT */

  /* Wait for TC flag to be raised for last char */
  while (!LL_USART_IsActiveFlag_TC(USART1))
  {
#if (USE_TIMEOUT == 1)
    /* Check Systick counter flag to decrement the time-out value */
    if (LL_SYSTICK_IsActiveCounterFlag())
    {
      if(Timeout-- == 0)
      {
        /* Time-out occurred. Set LED to blinking mode */
       // LED_Blinking(LED_BLINK_SLOW);
      }
    }
#endif /* USE_TIMEOUT */
  }

  //ubButtonPress =0;

  /* Turn LED2 On at end of transfer : Tx sequence completed successfully */
  //LED_On();
}




/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
