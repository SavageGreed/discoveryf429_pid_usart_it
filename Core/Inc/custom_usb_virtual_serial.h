
/**
  ******************************************************************************
  * @file           : custom_usb_virtual_serial.h
  * @version        : v1.0
  * @brief          : Header for serial access constants, data types and variables
  ******************************************************************************
  ******************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CUSTOM_USB_VIRTUAL_SERIAL_H__
#define __CUSTOM_USB_VIRTUAL_SERIAL_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/


/* Defines ------------------------------------------------------------------*/

#define VERBOSE_SWV 0//for debug output through STM32 SWV

#define  FIFO_SIZE 32  // must be 2^N
#define FIFO_INCR(x) (((x)+1)&((FIFO_SIZE)-1))

/* Types ------------------------------------------------------------------*/
/* Structure of FIFO*/
typedef struct FIFO
{
	uint32_t head;
	uint32_t tail;
	uint8_t data[FIFO_SIZE];
} FIFO;
extern FIFO RX_FIFO;

typedef enum
{
	CMD_STOP_ALL = 0,
	CMD_CHECK_VER,
	CMD_MODE_BIN,
	CMD_MODE_POS,
	CMD_MODE_BATCH,
	CMD_MODE_TXT
	//CMD_UPDATE_PARAMETERS,
	//CMD_UPDATE_TEST_MASK
}   SERIAL_CMD_t;

typedef struct PID_t
{
	uint16_t TargetPos;//	Target Output of the System (0..8190..16380)
	uint8_t nT;//  .. Number of Sampling cycles between each PID output
	float Inv_nT; //.. Inverse of nT
	float P;//	Kp .. Controller Gain Constant
	float I;//	Ti .. Controller Integration Constant
	float D;//	Td .. Controller Derivation Constant
//	float u_init;//	u0 .. Initial state of the integrator
//	float e_init;//	e0 .. Initial error
	uint8_t Update;
	uint8_t Enable;

} PID_t;


typedef struct BaseVoltage_t
{
	uint16_t code; //Voltage code: 0 - 4095
	uint8_t Update;
} BaseVoltage_t;


/* External variables ------------------------------------------------------------------*/

extern volatile SERIAL_CMD_t COM_Mode; // = CMD_STOP_ALL;
extern volatile uint8_t Get_Batch;

extern uint8_t TestInputMask[4];
extern PID_t PID_Param;
extern BaseVoltage_t NormVout;

/* Serial commands definitions------------------------------------------------------------------*/

//#define CMD_STR_STOP_ALL       "end" //Not using, since we want anything outside of defined string to top all
//#define CMD_CHECK_VER_STR      "checkver"
//#define CMD_CHECK_VER_STR_LEN (sizeof(CMD_CHECK_VER_STR)-1)

#define CMD_START_BIN_STR      "binary"
#define CMD_START_BIN_STR_LEN (sizeof(CMD_START_BIN_STR)-1)

#define CMD_START_POS_STR      "position"
#define CMD_START_POS_STR_LEN (sizeof(CMD_START_POS_STR)-1)

#define CMD_START_BATCH_STR    "bat"
#define CMD_START_BATCH_STR_LEN (sizeof(CMD_START_BATCH_STR)-1)

#define CMD_START_TXT_STR      "text"
#define CMD_START_TXT_STR_LEN (sizeof(CMD_START_TXT_STR)-1)

//#define CMD_MASK_OFF     "mask_off"
//#define CMD_MASK_LEN (sizeof(CMD_MASK_OFF)-1)
//#define CMD_MASK_PA1      "mask_PA1"
//#define CMD_MASK_PA2      "mask_PA2"
//#define CMD_MASK_PC1      "mask_PC1"
//#define CMD_MASK_PC2      "mask_PC2"


#define CMD_EN_PID      "En_PID"
#define CMD_EN_PID_LEN (sizeof(CMD_EN_PID)-1)

#define CMD_SET_PID      "set_PID"
#define CMD_SET_PID_LEN (sizeof(CMD_SET_PID)-1)
#define FLOAT_LEN 4 //(sizeof(float))
#define	PID_PARAM_LEN (1+FLOAT_LEN*3) //4 byte (32-bit float) * 3

#define CMD_SET_VOUT      "set_VOUT"
#define CMD_SET_VOUT_LEN (sizeof(CMD_SET_VOUT)-1)
#define UINT16_LEN 2 //(sizeof(uint16_t))
#define	VOUT_CODE_LEN (UINT16_LEN) //2 byte (16-bit unsigned integer)

#define CMD_SET_YPOS      "set_YPOS"
#define CMD_SET_YPOS_LEN (sizeof(CMD_SET_YPOS)-1)
#define	YPOS_CODE_LEN (UINT16_LEN) //2 byte (16-bit unsigned integer)


//static const size_t CmdStartBinLen = sizeof(CMD_STR_START_BIN)-1;
//static const size_t CmdStartTxtLen = sizeof(CMD_STR_START_TXT)-1;

//#define SERIAL_CMD_STOP_ALL       0x00000001U
//#define SERIAL_CMD_CHECK_VER      0x00000010U
//#define SERIAL_CMD_START_BIN      0x00000100U
//#define SERIAL_CMD_START_TXT      0x00001000U




#ifdef __cplusplus
}
#endif

#endif /* __CUSTOM_USB_VIRTUAL_SERIAL_H__ */

/*****************************END OF FILE****/
