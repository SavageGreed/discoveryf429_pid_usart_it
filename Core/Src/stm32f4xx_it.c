/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "UART_LL.h"
#include "custom_usb_virtual_serial.h"
#include "control_sys.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */
/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define aUSART1RxBuffer_SIZE CMD_SET_PID_LEN+PID_PARAM_LEN
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_adc1;
extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim6;

/* USER CODE BEGIN EV */
//#define MY_REC_TIMEOUT 0x00000FFF
//
//#define aUSART1RxBuffer_SIZE CMD_SET_PID_LEN+PID_PARAM_LEN
//uint8_t aUSART1RxBuffer[aUSART1RxBuffer_SIZE];

//uint16_t myCustomReceiveBuffer(USART_TypeDef* UART_PORT, uint8_t* framePtr,uint16_t frameSize);
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */
  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */
  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */
  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */
  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */
  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */
  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */
  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
  /* USER CODE END USART1_IRQn 0 */
  /* USER CODE BEGIN USART1_IRQn 1 */

	uint32_t len=0;
	  if ( LL_USART_IsActiveFlag_RXNE ( USART1 )  &&  LL_USART_IsEnabledIT_RXNE ( USART1 ) )
	  {
		  //USART_RX_Receive (USART1,&receiveBuffer[0]);
		 //len= myCustomReceiveBuffer(USART1,&aUSART1RxBuffer,aUSART1RxBuffer_SIZE);
		  len= UartRecWholeBuffer(USART1,&receiveBuffer,REC_BUFFER_SIZE);

			if        ( (len==CMD_START_BIN_STR_LEN) && (strncmp((const char *)receiveBuffer, CMD_START_BIN_STR, CMD_START_BIN_STR_LEN)==0) ) {
				COM_Mode = CMD_MODE_BIN;
			} else if ( (len==CMD_START_POS_STR_LEN) && (strncmp((const char *)receiveBuffer, CMD_START_POS_STR, CMD_START_POS_STR_LEN)==0) ) {
				COM_Mode = CMD_MODE_POS;
			} else if ( (len==CMD_START_BATCH_STR_LEN) && (strncmp((const char *)receiveBuffer, CMD_START_BATCH_STR, CMD_START_BATCH_STR_LEN)==0) ) {
				COM_Mode = CMD_MODE_BATCH;
				Get_Batch = 1;
			} else if ( (len==CMD_START_TXT_STR_LEN) && (strncmp((const char *)receiveBuffer, CMD_START_TXT_STR, CMD_START_TXT_STR_LEN)==0) ) {
				COM_Mode = CMD_MODE_TXT;
			} else if  (len==(CMD_EN_PID_LEN+1) && (strncmp((const char *)receiveBuffer, CMD_EN_PID, CMD_EN_PID_LEN)==0)) {
				PID_Param.Enable = receiveBuffer[CMD_EN_PID_LEN];
				len=CMD_EN_PID_LEN;
			} else if  (len==(CMD_SET_PID_LEN + PID_PARAM_LEN) && (strncmp((const char *)receiveBuffer, CMD_SET_PID, CMD_SET_PID_LEN)==0)) {

				PID_Param.nT = receiveBuffer[CMD_SET_PID_LEN];
				PID_Param.Inv_nT = 1.0f/receiveBuffer[CMD_SET_PID_LEN];
				PID_Param.P = bytes_to_float(&receiveBuffer[CMD_SET_PID_LEN+1]);
				PID_Param.I = bytes_to_float(&receiveBuffer[CMD_SET_PID_LEN+1+FLOAT_LEN]);
				PID_Param.D = bytes_to_float(&receiveBuffer[CMD_SET_PID_LEN+1+FLOAT_LEN*2]);
				PID_Param.Update = 1;
				len=CMD_SET_PID_LEN;

			} else if  (len==(CMD_SET_VOUT_LEN + VOUT_CODE_LEN) && (strncmp((const char *)receiveBuffer, CMD_SET_VOUT, CMD_SET_VOUT_LEN)==0)) {

				NormVout.code = bytes_to_uint16(&receiveBuffer[CMD_SET_VOUT_LEN]);
				NormVout.Update = 1;
				len=CMD_SET_VOUT_LEN;

			} else if  (len==(CMD_SET_YPOS_LEN + YPOS_CODE_LEN) && (strncmp((const char *)receiveBuffer, CMD_SET_YPOS, CMD_SET_YPOS_LEN)==0)) {

				PID_Param.TargetPos = bytes_to_uint16(&receiveBuffer[CMD_SET_YPOS_LEN]);
				//PID_Param.Update = 1;
				len=CMD_SET_YPOS_LEN;

			} else {
				COM_Mode = CMD_STOP_ALL;
			}

	  }
	  else
	  {
	    //Check if ERROR flag occure
	    if ( LL_USART_IsActiveFlag_ORE ( USART1 ) )  {
	        //errorCounter ++;
	    }
	    else  if ( LL_USART_IsActiveFlag_FE ( USART1 ) )  {
	        //errorCounter ++;
	    }
	    else  if ( LL_USART_IsActiveFlag_NE ( USART1 ) )  {
	        //errorCounter ++;
	    }
	  }
  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles TIM6 global interrupt, DAC1 and DAC2 underrun error interrupts.
  */
void TIM6_DAC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM6_DAC_IRQn 0 */
  /* USER CODE END TIM6_DAC_IRQn 0 */
  HAL_DAC_IRQHandler(&hdac);
  HAL_TIM_IRQHandler(&htim6);
  /* USER CODE BEGIN TIM6_DAC_IRQn 1 */
  /* USER CODE END TIM6_DAC_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream0 global interrupt.
  */
void DMA2_Stream0_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream0_IRQn 0 */
  /* USER CODE END DMA2_Stream0_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_adc1);
  /* USER CODE BEGIN DMA2_Stream0_IRQn 1 */
  /* USER CODE END DMA2_Stream0_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream2 global interrupt.
  */
void DMA2_Stream2_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream2_IRQn 0 */
  /* USER CODE END DMA2_Stream2_IRQn 0 */
  
  /* USER CODE BEGIN DMA2_Stream2_IRQn 1 */
  /* USER CODE END DMA2_Stream2_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream7 global interrupt.
  */
void DMA2_Stream7_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream7_IRQn 0 */
  /* USER CODE END DMA2_Stream7_IRQn 0 */
  
  /* USER CODE BEGIN DMA2_Stream7_IRQn 1 */
  /* USER CODE END DMA2_Stream7_IRQn 1 */
}

/* USER CODE BEGIN 1 */
/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
