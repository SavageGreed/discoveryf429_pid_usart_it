#ifndef UART_LL_H_
#define UART_LL_H_

#define REC_TIMEOUT 0x00000FFF
#define REC_BUFFER_SIZE 1024
#define CHECK_IF_DATA_SEND_UART_FINISHED(UART1) LL_USART_IsActiveFlag_TXE(UART1)
#define CHECK_IF_DATA_RECEIVE(UART1) 	LL_USART_IsActiveFlag_RXNE(UART1)


extern volatile uint8_t receiveBuffer[REC_BUFFER_SIZE];
extern volatile uint16_t ReceiveBufferPosition;

void UsartSendData(USART_TypeDef* UART_PORT, uint8_t* framePtr, uint16_t frameSize);
uint16_t UartRecWholeBuffer(USART_TypeDef* UART_PORT, uint8_t* framePtr,uint16_t frameSize);
void UartEnableInterrupt(USART_TypeDef* UART_PORT);


#endif /* UART_LL_H_ */
