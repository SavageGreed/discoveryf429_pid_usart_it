/*
 * control_sys.h
 *
 *  Created on: 14 May 2019
 *      Author: Justin Wong
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONTROL_SYS_FUNC
#define __CONTROL_SYS_FUNC

#ifdef __cplusplus
 extern "C" {
#endif

/* Exported functions ------------------------------------------------------- */
//float bytes_to_float(uint8_t *s);
// int16_t pid_controller(const uint16_t y, const uint16_t y_ref, const float dt, const int16_t out_max, const int16_t out_min,
// 						const PID_t param, const uint8_t reset);
#include "string.h"
/**
  * @brief   This function converts array of 4 bytes into a 32-bit floating point.
  * @param  s: pointer to the start address of byte array containing 4 bytes
  * @retval None
  */
 __always_inline inline float bytes_to_float(uint8_t *s) {
	float f;
	memcpy(&f, s, sizeof(f));
	return f;
	//*((float *) &temp);
}

 __always_inline inline float bytes_to_uint16(uint8_t *s) {
	uint16_t v;
	memcpy(&v, s, sizeof(v));
	return v;
}


#ifdef __cplusplus
}
#endif

#endif /* __CONTROL_SYS_FUNC */
